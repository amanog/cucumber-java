package step_definitions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import helpers.JunitParallel;
import org.junit.runner.RunWith;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import util.BrowserStackParallel;
import util.Browsers;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

@RunWith(JunitParallel.class)
public class Hooks {

    public static WebDriver driver;
    private static String driverDirectory = System.getProperty("user.dir") + "/webDrivers/usr/bin";
    private ChromeOptions chromeOptions = new ChromeOptions();
    private InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
    public static String downloadfolderPath = System.getProperty("user.dir") + "/testData/testDataOutput";
    private DesiredCapabilities capabilities = new DesiredCapabilities();
    private Browsers browsers = new Browsers();
    public static final String USERNAME = "";
    public static final String AUTOMATE_KEY = "";
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
    public  String platform = BrowserStackParallel.platform;
    public  String browserName = BrowserStackParallel.browserName;
    public  String browserVersion = BrowserStackParallel.browserVersion;


    @Before
    public void openBrowser() throws Exception {

        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        String os = System.getProperty("os.name").toLowerCase();

        String browser = System.getProperty("browser".toUpperCase());
        if (browser == null) {
            browser = System.getenv("browser");
            if (os.contains("win")) {
                browser = "chromeheadlesswindows";
            } else {
                browser = "chromeheadless";
            }
        }
        switch (browser.toLowerCase()) {

            case "chrome":
                HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
                chromePrefs.put("download.default_directory", downloadfolderPath);
                chromeOptions.setExperimentalOption("prefs", chromePrefs);
                // String os = System.getProperty("os.name").toLowerCase();

                if (os.contains("win")) {
                    System.setProperty("webdriver.chrome.driver", driverDirectory + "/chrome/chromedriver.exe");
                    driver = new ChromeDriver(chromeOptions);
                    driver.manage().window().maximize();
                    driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
                } else {

                    System.setProperty("webdriver.chrome.driver", driverDirectory + "/chrome/chromedriver");
                    chromeOptions.addArguments("--start-maximized");
                    driver = new ChromeDriver(chromeOptions);
                    driver.manage().window().setSize(new Dimension(1280, 1024));
                }
                break;

            case "chromeWindows":
                System.setProperty("webdriver.chrome.driver", driverDirectory + "/chrome/chromedriver.exe");
                chromeOptions.addArguments("--start-maximized");
                driver = new ChromeDriver(chromeOptions);
                driver.manage().window().setSize(new Dimension(1280, 1024));
                break;

            case "firefox":
                if (os.contains("win")) {
                    System.setProperty("webdriver.gecko.driver", driverDirectory + "/geckoFirefox/geckodriver.exe");
                    driver = new FirefoxDriver();
                    driver.manage().window().maximize();
                }
                else{
                    System.setProperty("webdriver.gecko.driver", driverDirectory + "/geckoFirefox/geckodriver");
                    driver = new FirefoxDriver();
                    driver.manage().window().setSize(new Dimension(1280, 1024));
                }
                break;

            case "chromeheadless":
                System.setProperty("webdriver.chrome.driver", driverDirectory + "/chrome/chromedriver");
                chromeOptions.addArguments("headless");
                chromeOptions.addArguments("window-size=1200x600");
                driver = new ChromeDriver(chromeOptions);
                break;

            case "chromeheadlesswindows":
                HashMap<String, Object> chromePrefsheadless = new HashMap<String, Object>();
                chromePrefsheadless.put("download.default_directory", downloadfolderPath);
                chromeOptions.setExperimentalOption("prefs", chromePrefsheadless);
                System.setProperty("webdriver.chrome.driver", driverDirectory + "/chrome/chromedriver.exe");
                chromeOptions.addArguments("headless");
                chromeOptions.addArguments("window-size=1200x600");
                driver = new ChromeDriver(chromeOptions);
                break;

            case "ie":
//                WebDriverManager.iedriver().setup();
                System.setProperty("webdriver.ie.driver", driverDirectory + "/IEDriver/IEDriverServer.exe");
                InternetExplorerOptions ieOptions = new InternetExplorerOptions().requireWindowFocus();
                ieOptions.destructivelyEnsureCleanSession();
                driver = new InternetExplorerDriver(ieOptions);
                driver.manage().window().setSize(new Dimension(1280, 1024));
                break;

            case "safari":
                driver = new SafariDriver();
                break;

            case "pjsmac":
                driver = Browsers.getPJSMacDriver();
                driver.manage().window().setSize(new Dimension(1280, 1024));
                break;

            case "pjslinux":
                driver = Browsers.getPJSLinux();
                break;

            case "pjswindows":
                driver = Browsers.pjsWindowsGet();
                driver.manage().window().setSize(new Dimension(1280, 1024));
                break;

            case "trifleJS":
                driver = Browsers.getTrifleJS();
                driver.manage().window().setSize(new Dimension(1280, 1024));
                break;

//          This can be only run from an windows machine.
            case "iegrid":
                String nodesURL = "http://0.0.0.0:5555/wd/hub";
                DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
                cap.setBrowserName("internet explorer");
                cap.setPlatform(Platform.WINDOWS);
                driver = new RemoteWebDriver(new URL(nodesURL), cap);
                break;

            case "htmlunit":
                driver = new HtmlUnitDriver();
                ((HtmlUnitDriver) driver).findElementByClassName("class").click();
                ((HtmlUnitDriver) driver).setJavascriptEnabled(true);

            case "browserstack":
                driver = Browsers.getBrowserStack("IE",
                        "11", "Windows", "10");
                break;

//            case "multiple":
//                new BrowserStackParallel();
//                break;

        }

        System.out.println("The Browser used for this test is: " + browser.toUpperCase());

    }


    @After
    public void embedScreenshot(Scenario scenario) throws Exception {
        System.out.println(scenario.getStatus()+"\t"+Arrays.asList(scenario.getSourceTagNames()).toString());
        if (scenario.isFailed()) {
            try {
                scenario.write("Current Page URL is " + new URL(driver.getCurrentUrl()));
                byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
                System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            }
        }
        if (driver != null) {
            driver.quit();
        }
    }
}